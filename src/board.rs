use std::str::FromStr;

pub mod check_mate;
pub mod fen_utils;
pub mod output;

use crate::board::fen_utils::{bit_castling, bit_en_passant, board};
use crate::figure::Figure;
use crate::figure_move::{can_move, Move, Pos};

// Castling constants.
// I use it for save state, which castle is
// possible to move. Exactly, I make
// bit operations for this thing
#[rustfmt::skip]
pub const CASTLING_WHITE_KING: i8  = 1i8 << 3;

#[rustfmt::skip]
pub const CASTLING_WHITE_QUEEN: i8 = 1i8 << 2;

#[rustfmt::skip]
pub const CASTLING_BLACK_KING: i8  = 1i8 << 1;

#[rustfmt::skip]
pub const CASTLING_BLACK_QUEEN: i8 = 1i8 << 0;

// Side of move
#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Side {
	White,
	Black,
	Null,
}

// w is Side::White,
// b is Side::Black
impl FromStr for Side {
	type Err = ();

	fn from_str(input: &str) -> Result<Side, ()> {
		match input {
			"w" => Ok(Side::White),
			"b" => Ok(Side::Black),
			_ => Err(()),
		}
	}
}

// Promotion is pawn promotion
#[derive(Clone)]
pub enum StateChess {
	Check,
	Checkmate,
	Promotion,
	Null,
}

// En passant is i8,
// cause I can save
// chess pos in 2^6 (64 squares)
#[derive(Clone)]
pub struct Chess {
	pub board: Vec<Vec<Figure>>,
	pub w: i8,
	pub h: i8,
	pub sidemove: Side,
	pub castling: i8,
	pub en_passant: i8,
	pub state: StateChess,
	pub kingspos: [i8; 2],
}

impl Chess {
	pub fn fig_at(&self, pos: &Pos) -> Option<Figure> {
		let x: i8 = pos.x;
		let y: i8 = pos.y;

		if !(y >= 0 && y < self.board.len() as i8) {
			return None;
		}

		if !(x >= 0 && x < self.board[y as usize].len() as i8) {
			return None;
		}

		Some(self.board[y as usize][x as usize])
	}

	pub fn fig_set(&mut self, pos: &Pos, figure: Figure) {
		let x: i8 = pos.x;
		let y: i8 = pos.y;

		if !(y >= 0 && y < self.board.len() as i8) {
			return;
		}

		if !(x >= 0 && x < self.board[y as usize].len() as i8) {
			return;
		}

		self.board[y as usize][x as usize] = figure;
	}

	pub fn from_fen(fen: &str) -> Option<Self> {
		let fen_split: Vec<&str> = fen.split(" ").collect();

		if fen_split.len() != 6 {
			eprintln!("fen len error");
			return None;
		}

		let _board: Vec<Vec<Figure>> = board(fen_split[0]).expect("board error");

		let board_h: i8 = _board.len() as i8;
		let board_w: i8 = _board[0].len() as i8;

		let mut _kingspos: [i8; 2] = [-1, -1];
		for i in 0.._board[0].len() {
			for j in 0.._board.len() {
				if _board[j][i].name == 'K' && _board[j][i].side == Side::White {
					_kingspos[0] = (i + j * _board[0].len()) as i8;
				}
				if _board[j][i].name == 'K' && _board[j][i].side == Side::Black {
					_kingspos[1] = (i + j * _board[0].len()) as i8;
				}
			}
		}
		if _kingspos[0] == -1 || _kingspos[1] == -1 {
			eprintln!("kingspos error {:?}", _kingspos);
			eprintln!("board: {:#?}", _board);
			return None;
		}

		let side: Side = Side::from_str(fen_split[1]).expect("side error");

		let bit_castling: i8 = bit_castling(fen_split[2]) & 0b01111111 as i8;

		let bit_en_passant: i8 = bit_en_passant(fen_split[3], board_w, board_h)
			.expect("en passant error")
			& 0b01111111 as i8;

		Some(Self {
			board: _board,
			w: board_w,
			h: board_h,
			sidemove: side,
			castling: bit_castling,
			en_passant: bit_en_passant,
			state: StateChess::Null,
			kingspos: _kingspos,
		})
	}

	pub fn is_possible_move(&self, figure_move: Move) -> bool {
		let possible_moves: Vec<Move> =
			(self.fig_at(&figure_move.a).unwrap().generate_moves)(self, figure_move.a);

		for possible_move in possible_moves.clone() {
			if possible_move == figure_move {
				return true;
			}
		}

		eprintln!("! Possible moves by this figure: {:?} !", possible_moves);
		false
	}

	pub fn move_figure(&mut self, figure_move: &Move) -> Result<(), ()> {
		let moved_figure: Option<Figure> = self.fig_at(&figure_move.a);
		if moved_figure == None {
			return Err(());
		}
		let moved_figure: Figure = moved_figure.unwrap();

		self.fig_set(&figure_move.b, moved_figure);
		self.fig_set(&figure_move.a, Figure::null());

		(moved_figure.extra_move)(self, &figure_move);
		if self.en_passant & 0b01000000 == 0 {
			self.en_passant = 0;
		}
		if self.en_passant & 0b01000000 != 0 {
			self.en_passant &= 0b00111111;
		}

		return Ok(());
	}

	pub fn stdin_user_move(&mut self) {
		println!("Enter a move (like g2g4): ");

		let mut user_move_str: String = String::new();
		std::io::stdin()
			.read_line(&mut user_move_str)
			.expect("failed to readline");

		let user_move = Move::from_str(&user_move_str).unwrap();

		self.user_move_figure(user_move);
	}

	// !TODO pawn Promotion
	pub fn user_move_figure(&mut self, user_move: Move) {
		let before = std::time::Instant::now();

		println!("Can move time: {:.2?}", before.elapsed());

		let before = std::time::Instant::now();

		if !self.is_possible_move(user_move) {
			eprintln!("! Impossible move (check_move): {} !", user_move);
			return;
		}

		println!("check_move time: {:.2?}", before.elapsed());

		let before = std::time::Instant::now();

		self.move_figure(&user_move).expect("Move_figure error");

		println!("move_figure time: {:.2?}", before.elapsed());

		self.sidemove = if self.sidemove == Side::White {
			Side::Black
		} else {
			Side::White
		};
	}
}
