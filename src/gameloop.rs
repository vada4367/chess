#![allow(dead_code)]

use crate::board::{Chess, Side};
use crate::figure_move::{Move, Pos};
use core::str::FromStr;
use macroquad::prelude::*;

pub async fn chess_macroquad_gameloop(mut chess: Chess) {
	let board_texture = Texture2D::from_file_with_format(
		include_bytes!("../assets/board_theme.png"),
		Some(ImageFormat::Png),
	);
	let mut piece_set: Vec<Texture2D> = Vec::new();
	for texture_bytes in TEXTURES_OF_FIGURES {
		piece_set.push(Texture2D::from_file_with_format(
			texture_bytes,
			Some(ImageFormat::Png),
		));
	}
	let mut possible_moves: Vec<Move> = Vec::new();

	chess.user_move_figure(Move::from_str("d2d3").unwrap());
	chess.user_move_figure(Move::from_str("c7c6").unwrap());
	chess.user_move_figure(Move::from_str("c1f4").unwrap());
	chess.user_move_figure(Move::from_str("d8a5").unwrap());
	//println!(
	//	"{}",
	//	recursive_counter(chess.clone(), 1, 1, &board_texture, &piece_set).await
	//);
	loop {
		input_game(&mut chess, &mut possible_moves);

		draw_game(&chess, &possible_moves, &board_texture, &piece_set);
		next_frame().await
	}
}

async fn recursive_counter(
	chess: Chess,
	depth: u8,
	debug_moves_on_depth: u8,
	board_texture: &Texture2D,
	piece_set: &Vec<Texture2D>,
) -> u64 {
	if depth == 0 {
		return 1;
	}

	let mut result: u64 = 0;
	for i in 0..chess.w {
		for j in 0..chess.h {
			for possible_move in (chess
				.fig_at(&Pos {
					x: i,
					y: j,
				})
				.unwrap()
				.generate_moves)(
				&chess,
				Pos {
					x: i,
					y: j,
				},
			) {
				let mut chess_copy = chess.clone();

				chess_copy
					.move_figure(&possible_move)
					.expect("Move_figure error");

				chess_copy.sidemove = if chess_copy.sidemove == Side::White {
					Side::Black
				} else {
					Side::White
				};

				if depth == debug_moves_on_depth {
					println!(
						"{}: {}",
						possible_move,
						Box::pin(recursive_counter(
							chess_copy.clone(),
							depth - 1,
							debug_moves_on_depth,
							board_texture,
							piece_set
						))
						.await
					);
				}

				result += Box::pin(recursive_counter(
					chess_copy.clone(),
					depth - 1,
					debug_moves_on_depth,
					board_texture,
					piece_set,
				))
				.await;

				//draw_game(&chess_copy, &vec![], board_texture, piece_set);
				//std::thread::sleep(std::time::Duration::from_millis(1000));

				//next_frame().await
			}
		}
	}

	result
}

fn draw_game(
	chess: &Chess,
	possible_moves: &Vec<Move>,
	board_texture: &Texture2D,
	piece_set: &Vec<Texture2D>,
) {
	clear_background(Color {
		r: 0.11,
		g: 0.11,
		b: 0.11,
		a: 1.0,
	});

	let cell_size = min(screen_width(), screen_height()) / max(chess.w as f32, chess.h as f32);

	draw_texture_ex(
		board_texture,
		0f32,
		0f32,
		WHITE,
		DrawTextureParams {
			dest_size: Some(Vec2 {
				x: cell_size * chess.w as f32,
				y: cell_size * chess.h as f32,
			}),
			..Default::default()
		},
	);

	for i in 0..chess.w {
		for j in 0..chess.h {
			let fig_at_texture = INDEXES_OF_FIGURES.iter().position(|&r| {
				r == chess
					.fig_at(&Pos {
						x: i,
						y: j,
					})
					.unwrap()
					.name
			});

			if fig_at_texture.is_none() {
				continue;
			}

			let fig_at_texture = &piece_set[fig_at_texture.unwrap() * 2
				+ if chess
					.fig_at(&Pos {
						x: i,
						y: j,
					})
					.unwrap()
					.side == Side::Black
				{
					1
				} else {
					0
				}];

			draw_texture_ex(
				fig_at_texture,
				i as f32 * cell_size,
				j as f32 * cell_size,
				WHITE,
				DrawTextureParams {
					dest_size: Some(Vec2 {
						x: cell_size,
						y: cell_size,
					}),
					..Default::default()
				},
			);
		}
	}

	for possible_move in possible_moves {
		draw_circle(
			possible_move.b.x as f32 * cell_size + 0.5 * cell_size,
			possible_move.b.y as f32 * cell_size + 0.5 * cell_size,
			cell_size * 0.15,
			Color {
				r: GREEN.r,
				g: GREEN.g,
				b: GREEN.b,
				a: 0.5,
			},
		);
	}
}

fn input_game(chess: &mut Chess, possible_moves: &mut Vec<Move>) {
	let cell_size = min(screen_width(), screen_height()) / max(chess.w as f32, chess.h as f32);

	if is_mouse_button_pressed(MouseButton::Left) {
		let figure_pos = Pos {
			x: (mouse_position().0 / cell_size) as i8,
			y: (mouse_position().1 / cell_size) as i8,
		};

		if mouse_position().0 < chess.w as f32 * cell_size
			&& mouse_position().1 < chess.h as f32 * cell_size
			&& possible_moves.len() != 0
		{
			let user_move = Move {
				a: possible_moves[0].a,
				b: figure_pos,
			};

			chess.user_move_figure(user_move);
			*possible_moves = Vec::new() as Vec<Move>;
		}

		if mouse_position().0 < chess.w as f32 * cell_size
			&& mouse_position().1 < chess.h as f32 * cell_size
			&& possible_moves.len() == 0
		{
			*possible_moves =
				(chess.fig_at(&figure_pos).unwrap().generate_moves)(&chess, figure_pos);
		}
	}
}

fn max(a: f32, b: f32) -> f32 {
	if a > b {
		return a;
	}
	b
}
fn min(a: f32, b: f32) -> f32 {
	if a < b {
		return a;
	}
	b
}

static TEXTURES_OF_FIGURES: &[&[u8]] = &[
	include_bytes!("../assets/wP.png"),
	include_bytes!("../assets/bP.png"),
	include_bytes!("../assets/wB.png"),
	include_bytes!("../assets/bB.png"),
	include_bytes!("../assets/wN.png"),
	include_bytes!("../assets/bN.png"),
	include_bytes!("../assets/wR.png"),
	include_bytes!("../assets/bR.png"),
	include_bytes!("../assets/wQ.png"),
	include_bytes!("../assets/bQ.png"),
	include_bytes!("../assets/wK.png"),
	include_bytes!("../assets/bK.png"),
];

static INDEXES_OF_FIGURES: &[char] = &['P', 'B', 'N', 'R', 'Q', 'K'];
