use crate::figure::{null_extra_move, Chess, Figure, Side};
use crate::figure_move::{vhd_moves, Move, Pos};

impl Figure {
	pub fn queen(_side: Side) -> Self {
		Self {
			name: 'Q',
			side: _side,
			generate_moves: queen_generate_moves,
			extra_move: null_extra_move,
		}
	}
}

fn queen_generate_moves(chess: &Chess, pos: Pos) -> Vec<Move> {
	let mut result: Vec<Move> = Vec::new();

	let x: i8 = pos.x;
	let y: i8 = pos.y;

	// Like rook
	result.extend(vhd_moves(chess, x, y, -1, 0));
	result.extend(vhd_moves(chess, x, y, 1, 0));
	result.extend(vhd_moves(chess, x, y, 0, 1));
	result.extend(vhd_moves(chess, x, y, 0, -1));

	// Like bishop
	result.extend(vhd_moves(chess, x, y, -1, -1));
	result.extend(vhd_moves(chess, x, y, -1, 1));
	result.extend(vhd_moves(chess, x, y, 1, -1));
	result.extend(vhd_moves(chess, x, y, 1, 1));

	return result;
}
