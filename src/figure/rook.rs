use crate::figure::{Chess, Figure, Side};
use crate::figure_move::{vhd_moves, Move, Pos};

use crate::board::{
	CASTLING_BLACK_KING, CASTLING_BLACK_QUEEN, CASTLING_WHITE_KING, CASTLING_WHITE_QUEEN,
};

impl Figure {
	pub fn rook(_side: Side) -> Self {
		Self {
			name: 'R',
			side: _side,
			generate_moves: rook_generate_moves,
			extra_move: rook_extra_move,
		}
	}
}

fn rook_generate_moves(chess: &Chess, pos: Pos) -> Vec<Move> {
	let mut result: Vec<Move> = Vec::new();

	let x: i8 = pos.x;
	let y: i8 = pos.y;

	result.extend(vhd_moves(chess, x, y, -1, 0));
	result.extend(vhd_moves(chess, x, y, 1, 0));
	result.extend(vhd_moves(chess, x, y, 0, 1));
	result.extend(vhd_moves(chess, x, y, 0, -1));

	return result;
}

fn rook_extra_move(chess: &mut Chess, rook_move: &Move) {
	if rook_move.a.x == 0 && rook_move.a.y == 0 && chess.sidemove == Side::Black {
		chess.castling &= 0b01111111 ^ CASTLING_BLACK_QUEEN;
	}

	if rook_move.a.x == 0 && rook_move.a.y == chess.h - 1 && chess.sidemove == Side::White {
		chess.castling &= 0b01111111 ^ CASTLING_WHITE_QUEEN;
	}

	if rook_move.a.x == chess.w && rook_move.a.y == 0 && chess.sidemove == Side::Black {
		chess.castling &= 0b01111111 ^ CASTLING_BLACK_KING;
	}

	if rook_move.a.x == chess.w && rook_move.a.y == chess.h - 1 && chess.sidemove == Side::White {
		chess.castling &= 0b01111111 ^ CASTLING_WHITE_KING;
	}
}
