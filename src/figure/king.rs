use crate::figure::{Chess, Figure, Side};
use crate::figure_move::{can_move, Move, Pos};

use crate::board::{
	CASTLING_BLACK_KING, CASTLING_BLACK_QUEEN, CASTLING_WHITE_KING, CASTLING_WHITE_QUEEN,
};

impl Figure {
	pub fn king(_side: Side) -> Self {
		Self {
			name: 'K',
			side: _side,
			generate_moves: king_generate_moves,
			extra_move: king_extra_move,
		}
	}
}

fn king_generate_moves(chess: &Chess, pos: Pos) -> Vec<Move> {
	let mut result: Vec<Move> = Vec::new();

	let king: usize = if chess.sidemove == Side::White { 0 } else { 1 };

	let king_pos: Pos = Pos {
		x: chess.kingspos[king] % chess.w,
		y: chess.kingspos[king] / chess.w,
	};

	if king_pos != pos {
		return Vec::new();
	}

	let possible_moves = vec![
		Move::new(king_pos.x, king_pos.y, king_pos.x + 1, king_pos.y),
		Move::new(king_pos.x, king_pos.y, king_pos.x - 1, king_pos.y),
		Move::new(king_pos.x, king_pos.y, king_pos.x + 1, king_pos.y + 1),
		Move::new(king_pos.x, king_pos.y, king_pos.x - 1, king_pos.y - 1),
		Move::new(king_pos.x, king_pos.y, king_pos.x - 1, king_pos.y + 1),
		Move::new(king_pos.x, king_pos.y, king_pos.x + 1, king_pos.y - 1),
		Move::new(king_pos.x, king_pos.y, king_pos.x, king_pos.y + 1),
		Move::new(king_pos.x, king_pos.y, king_pos.x, king_pos.y - 1),
		// Castling
		Move::new(king_pos.x, king_pos.y, king_pos.x - 2, king_pos.y),
		Move::new(king_pos.x, king_pos.y, king_pos.x + 2, king_pos.y),
	];

	for king_move in possible_moves {
		if !can_move(chess, &king_move) {
			continue;
		}

		if !check_castling(chess, &king_move) {
			continue;
		}

		result.push(king_move);
	}

	result
}

fn king_extra_move(chess: &mut Chess, king_move: &Move) {
	if chess.sidemove == Side::White {
		chess.castling &= 0b01111111 ^ CASTLING_WHITE_KING;
		chess.castling &= 0b01111111 ^ CASTLING_WHITE_QUEEN;
	}

	if chess.sidemove == Side::Black {
		chess.castling &= 0b01111111 ^ CASTLING_BLACK_KING;
		chess.castling &= 0b01111111 ^ CASTLING_BLACK_QUEEN;
	}

	let mut rook_old_pos = Pos {
		x: chess.w,
		y: king_move.b.y,
	};
	if king_move.dx() == 2 {
		let rook_new_pos = Pos {
			x: king_move.b.x - 1,
			y: king_move.b.y,
		};
		chess.fig_set(&rook_new_pos, Figure::rook(chess.sidemove));
	}
	if king_move.dx() == -2 {
		rook_old_pos = Pos {
			x: 0,
			y: king_move.b.y,
		};
		let rook_new_pos = Pos {
			x: king_move.b.x + 1,
			y: king_move.b.y,
		};
		chess.fig_set(&rook_new_pos, Figure::rook(chess.sidemove));
	}

	if king_move.dx().abs() == 2 {
		chess.fig_set(&rook_old_pos, Figure::null());
	}

	let mut king_index = 0;
	if chess.sidemove == Side::Black {
		king_index = 1;
	}
	chess.kingspos[king_index] = king_move.b.x + king_move.b.y * chess.w;
}

fn check_castling(chess: &Chess, king_move: &Move) -> bool {
	if king_move.dx().abs() == 2 && chess.is_check() {
		return false;
	}

	// Check king side castling (O-O)
	if king_move.dx() == 2 {
		if !can_move(
			chess,
			&Move {
				a: king_move.a,
				b: Pos {
					x: king_move.b.x + 1,
					y: king_move.b.y,
				},
			},
		) {
			return false;
		}
		if chess
			.fig_at(&Pos {
				x: chess.w - 1,
				y: king_move.b.y,
			})
			.unwrap()
			.side != chess.sidemove
		{
			return false;
		}
		if chess.sidemove == Side::White && chess.castling & CASTLING_WHITE_KING == 0 {
			return false;
		}
		if chess.sidemove == Side::Black && chess.castling & CASTLING_BLACK_KING == 0 {
			return false;
		}
	}
	// Check queen side castling (O-O-O)
	if king_move.dx() == -2 {
		if chess
			.fig_at(&Pos {
				x: 1,
				y: king_move.a.y,
			})
			.unwrap()
			.name != ' '
		{
			return false;
		}
		if !can_move(
			chess,
			&Move {
				a: king_move.a,
				b: Pos {
					x: king_move.b.x - 1,
					y: king_move.b.y,
				},
			},
		) {
			return false;
		}
		if chess
			.fig_at(&Pos {
				x: 0,
				y: king_move.b.y,
			})
			.unwrap()
			.side != chess.sidemove
		{
			return false;
		}
		if chess.sidemove == Side::White && chess.castling & CASTLING_WHITE_QUEEN == 0 {
			return false;
		}
		if chess.sidemove == Side::Black && chess.castling & CASTLING_BLACK_QUEEN == 0 {
			return false;
		}
	}

	true
}
