use crate::figure::{null_extra_move, Chess, Figure, Side};
use crate::figure_move::{vhd_moves, Move, Pos};

impl Figure {
	pub fn bishop(_side: Side) -> Self {
		Self {
			name: 'B',
			side: _side,
			generate_moves: bishop_generate_moves,
			extra_move: null_extra_move,
		}
	}
}

fn bishop_generate_moves(chess: &Chess, pos: Pos) -> Vec<Move> {
	let mut result: Vec<Move> = Vec::new();

	let x: i8 = pos.x;
	let y: i8 = pos.y;

	result.extend(vhd_moves(chess, x, y, -1, -1));
	result.extend(vhd_moves(chess, x, y, -1, 1));
	result.extend(vhd_moves(chess, x, y, 1, -1));
	result.extend(vhd_moves(chess, x, y, 1, 1));

	return result;
}
