use crate::board::StateChess;
use crate::figure::{Chess, Figure, Side};
use crate::figure_move::{can_move, Move, Pos};

impl Figure {
	pub fn pawn(_side: Side) -> Self {
		Self {
			name: 'P',
			side: _side,
			generate_moves: pawn_generate_moves,
			extra_move: pawn_extra_move,
		}
	}
}

fn pawn_generate_moves(chess: &Chess, pos: Pos) -> Vec<Move> {
	let up: i8 = if chess.sidemove == Side::White { -1 } else { 1 };

	let allow_moves: Vec<Move> = vec![
		Move::new(pos.x, pos.y, pos.x, pos.y + up),
		Move::new(pos.x, pos.y, pos.x + 1, pos.y + up),
		Move::new(pos.x, pos.y, pos.x - 1, pos.y + up),
		Move::new(pos.x, pos.y, pos.x, pos.y + 2 * up),
	];

	let mut result: Vec<Move> = Vec::new();

	for pawn_move in allow_moves {
		let pawn_moved_end = chess.fig_at(&pawn_move.b);
		if pawn_moved_end == None {
			continue;
		}
		let pawn_moved_end = pawn_moved_end.unwrap();

		if !can_move(chess, &pawn_move) {
			continue;
		}
		if pawn_move.dy().abs() == 2
			&& (chess.sidemove == Side::White && pawn_move.a.y != 6
				|| chess.sidemove == Side::Black && pawn_move.a.y != 1)
		{
			continue;
		}
		if pawn_move.dy().abs() == 2 && !pawn_moved_end.is_null() {
			continue;
		}
		if pawn_move.dy().abs() == 2
			&& !chess
				.fig_at(&Pos {
					x: pawn_move.b.x,
					y: pawn_move.b.y - up,
				})
				.unwrap()
				.is_null()
		{
			continue;
		}
		if pawn_move.dx().abs() == 1
			&& pawn_move.b.x + (pawn_move.b.y - up) * chess.w == chess.en_passant & 0b00111111
		{
			result.push(pawn_move);
			continue;
		}
		if pawn_move.dx().abs() == 1 && pawn_moved_end.side == Side::Null {
			continue;
		}
		if pawn_move.dy().abs() == 1 && pawn_move.dx() == 0 && !pawn_moved_end.is_null() {
			continue;
		}

		result.push(pawn_move);
	}

	return result;
}

fn pawn_extra_move(chess: &mut Chess, pawn_move: &Move) {
	let up: i8 = if chess.sidemove == Side::White { -1 } else { 1 };

	// en_passant
	if pawn_move.dy().abs() == 2 {
		// | 0b01000000 because I
		// need to delete en passant
		// after one move. So its
		// good counter
		chess.en_passant = (pawn_move.b.x + pawn_move.b.y * chess.w) | 0b01000000;
	}

	if pawn_move.dy().abs() == 1
		&& pawn_move.dx().abs() == 1
		&& pawn_move.b.x + (pawn_move.b.y - up) * chess.w == chess.en_passant & 0b00111111
	{
		let eat_en_passant = Pos {
			x: pawn_move.b.x,
			y: pawn_move.b.y - up,
		};

		chess.fig_set(&eat_en_passant, Figure::null());
	}

	// Promotion
	if pawn_move.b.y == chess.h || pawn_move.b.y == 0 {
		chess.state = StateChess::Promotion;
	}
}
