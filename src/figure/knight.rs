use crate::figure::{null_extra_move, Chess, Figure, Side};
use crate::figure_move::{can_move, Move, Pos};

impl Figure {
	pub fn knight(_side: Side) -> Self {
		Self {
			name: 'N',
			side: _side,
			generate_moves: knight_generate_moves,
			extra_move: null_extra_move,
		}
	}
}

fn knight_generate_moves(chess: &Chess, pos: Pos) -> Vec<Move> {
	let allow_moves: Vec<Move> = vec![
		Move::new(pos.x, pos.y, pos.x + 1, pos.y + 2),
		Move::new(pos.x, pos.y, pos.x + 1, pos.y - 2),
		Move::new(pos.x, pos.y, pos.x - 1, pos.y + 2),
		Move::new(pos.x, pos.y, pos.x - 1, pos.y - 2),
		Move::new(pos.x, pos.y, pos.x + 2, pos.y + 1),
		Move::new(pos.x, pos.y, pos.x + 2, pos.y - 1),
		Move::new(pos.x, pos.y, pos.x - 2, pos.y + 1),
		Move::new(pos.x, pos.y, pos.x - 2, pos.y - 1),
	];

	let mut result: Vec<Move> = Vec::new();

	for knight_move in allow_moves {
		if chess.fig_at(&knight_move.b) == None {
			continue;
		}

		if !can_move(chess, &knight_move) {
			continue;
		}

		result.push(knight_move);
	}

	return result;
}
