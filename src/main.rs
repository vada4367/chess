pub const EPRINTLN: bool = false;

pub mod board;
pub mod figure;
pub mod figure_move;

pub mod gameloop;
use crate::gameloop::chess_macroquad_gameloop;

use crate::board::Chess;

#[macroquad::main("RustChess")]
async fn main() {
	let chess_board = Chess::from_fen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
		.expect("FEN error");

	chess_macroquad_gameloop(chess_board).await;
	/*
	loop {
		println!("{}", chess_board);
		chess_board.user_move_figure();
	}
	*/
}
