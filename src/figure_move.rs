use crate::board::Chess;
use crate::figure::Figure;
use crate::EPRINTLN;
use core::str::FromStr;

#[derive(PartialEq, Clone, Copy, Debug)]
pub struct Pos {
	pub x: i8,
	pub y: i8,
}

#[derive(PartialEq, Clone, Copy, Debug)]
pub struct Move {
	pub a: Pos,
	pub b: Pos,
}

impl FromStr for Move {
	type Err = ();

	fn from_str(input: &str) -> Result<Move, ()> {
		Ok(Move {
			a: Pos {
				x: (input.chars().nth(0).unwrap() as u32 - 'a' as u32) as i8,
				y: 7 - (input.chars().nth(1).unwrap() as u32 - '1' as u32) as i8,
			},
			b: Pos {
				x: (input.chars().nth(2).unwrap() as u32 - 'a' as u32) as i8,
				y: 7 - (input.chars().nth(3).unwrap() as u32 - '1' as u32) as i8,
			},
		})
	}
}

impl std::fmt::Display for Move {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(
			f,
			"{}{}{}{}",
			char::from_u32(self.a.x as u32 + 'a' as u32).unwrap(),
			8 - self.a.y,
			char::from_u32(self.b.x as u32 + 'a' as u32).unwrap(),
			8 - self.b.y
		)
	}
}

impl Move {
	pub fn new<T: Into<i8>>(x1: T, y1: T, x2: T, y2: T) -> Self {
		Self {
			a: Pos {
				x: x1.into(),
				y: y1.into(),
			},
			b: Pos {
				x: x2.into(),
				y: y2.into(),
			},
		}
	}

	pub fn dx(&self) -> i8 {
		self.b.x - self.a.x
	}

	pub fn dy(&self) -> i8 {
		self.b.y - self.a.y
	}
}

pub fn can_move_without_check(chess: &Chess, unsafe_move: &Move) -> bool {
	let moved_figure: Option<Figure> = chess.fig_at(&unsafe_move.a);
	let ate_figure: Option<Figure> = chess.fig_at(&unsafe_move.b);

	if moved_figure == None || ate_figure == None {
		if EPRINTLN {
			eprintln!("! not to be moved figure !");
		}
		return false;
	}

	let moved_figure: Figure = moved_figure.unwrap();
	let ate_figure: Figure = ate_figure.unwrap();

	if moved_figure.side != chess.sidemove || ate_figure.side == chess.sidemove {
		if EPRINTLN {
			eprintln!("! Move by not your figure, or you will eat your figure !");
			eprintln!(
				"! moved_figure: {:?} ate_figure: {:?} !",
				moved_figure, ate_figure
			);
		}
		return false;
	}

	true
}

pub fn can_move(chess: &Chess, unsafe_move: &Move) -> bool {
	if !can_move_without_check(chess, unsafe_move) {
		return false;
	}

	let mut check_check: Chess = chess.clone();

	check_check
		.move_figure(unsafe_move)
		.expect("Move figure error");

	if check_check.is_check() {
		if EPRINTLN {
			eprintln!("! Impossible move (check king) !");
		}
		return false;
	}

	true
}

// Vertical, horizontal, diagonal moves for bishop, rook and queen
pub fn vhd_moves(chess: &Chess, x: i8, y: i8, dx: i8, dy: i8) -> Vec<Move> {
	let mut result: Vec<Move> = Vec::new();

	let mut i: i8 = 0;

	let mut check_check: Chess;
	let mut vhd_move: Move;
	loop {
		i += 1;
		vhd_move = Move {
			a: Pos {
				x,
				y,
			},
			b: Pos {
				x: x + i * dx,
				y: y + i * dy,
			},
		};

		if !can_move_without_check(chess, &vhd_move) {
			break;
		}
		check_check = chess.clone();
		check_check
			.move_figure(&vhd_move)
			.expect("Move figure error");
		if check_check.is_check() {
			continue;
		}
		if chess.fig_at(&vhd_move.b).unwrap().name != ' ' {
			if chess.fig_at(&vhd_move.b).unwrap().side != chess.sidemove {
				result.push(vhd_move);
			}
			break;
		}

		result.push(vhd_move);
	}

	result
}
