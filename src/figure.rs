// All figures realisations
pub mod bishop;
pub mod king;
pub mod knight;
pub mod pawn;
pub mod queen;
pub mod rook;

use crate::board::{Chess, Side};
use crate::figure_move::{Move, Pos};

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Figure {
	pub name: char,
	pub side: Side,
	pub generate_moves: fn(&Chess, Pos) -> Vec<Move>,
	pub extra_move: fn(&mut Chess, &Move),
}

impl Figure {
	pub fn null() -> Self {
		Self {
			name: ' ',
			side: Side::Null,
			generate_moves: null_generate_moves,
			extra_move: null_extra_move,
		}
	}

	pub fn is_null(&self) -> bool {
		return self.name == ' ';
	}
}

fn null_generate_moves(_: &Chess, __: Pos) -> Vec<Move> {
	Vec::new()
}

fn null_extra_move(_: &mut Chess, __: &Move) {
	();
}
