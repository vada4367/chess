use crate::board::{Chess, Side};
use crate::figure_move::{Move, Pos};

impl Chess {
	pub fn is_check(&self) -> bool {
		let king: usize = if self.sidemove == Side::White { 0 } else { 1 };

		let king_x: i8 = self.kingspos[king] % self.w;
		let king_y: i8 = self.kingspos[king] / self.w;

		// Vertical, gorizontal and diagonal check check (Queen, Rook, Bishop)
		if check_brq(self, king_x, king_y, -1, -1)
			|| check_brq(self, king_x, king_y, -1, 1)
			|| check_brq(self, king_x, king_y, 1, -1)
			|| check_brq(self, king_x, king_y, 1, 1)
			|| check_brq(self, king_x, king_y, 0, 1)
			|| check_brq(self, king_x, king_y, 0, -1)
			|| check_brq(self, king_x, king_y, 1, 0)
			|| check_brq(self, king_x, king_y, -1, 0)
		{
			return true;
		}

		// Pawns
		let up: i8 = if self.sidemove == Side::White { -1 } else { 1 };

		let left_pawn = self.fig_at(&Pos {
			x: king_x - 1,
			y: king_y + up,
		});
		if left_pawn != None {
			let left_pawn = left_pawn.unwrap();

			if left_pawn.name == 'P' && left_pawn.side != self.sidemove {
				return true;
			}
		}

		let right_pawn = self.fig_at(&Pos {
			x: king_x + 1,
			y: king_y + up,
		});
		if right_pawn != None {
			let right_pawn = right_pawn.unwrap();

			if right_pawn.name == 'P' && right_pawn.side != self.sidemove {
				return true;
			}
		}

		check_knight(
			self,
			Pos {
				x: king_x,
				y: king_y,
			},
		)
	}
}

// Bishop Rook Queen
fn check_brq(chess: &Chess, king_x: i8, king_y: i8, dx: i8, dy: i8) -> bool {
	let (mut x, mut y): (i8, i8) = (king_x + dx, king_y + dy);

	while x >= 0 && y >= 0 && x < chess.w && y < chess.h {
		let checking_figure = chess
			.fig_at(&Pos {
				x,
				y,
			})
			.unwrap();

		if checking_figure.side == chess.sidemove {
			return false;
		}

		if checking_figure.name == 'P'
			|| checking_figure.name == 'K'
			|| checking_figure.name == 'N'
		{
			return false;
		}

		if checking_figure.name == 'R' && !(dx == 0 || dy == 0) {
			return false;
		}
		if checking_figure.name == 'B' && (dx == 0 || dy == 0) {
			return false;
		}

		if checking_figure.side != chess.sidemove && checking_figure.name != ' ' {
			return true;
		}

		x += dx;
		y += dy;
	}

	return false;
}

fn check_knight(chess: &Chess, pos: Pos) -> bool {
	let knight_moves: Vec<Move> = vec![
		Move::new(pos.x, pos.y, pos.x + 1, pos.y + 2),
		Move::new(pos.x, pos.y, pos.x + 1, pos.y - 2),
		Move::new(pos.x, pos.y, pos.x - 1, pos.y + 2),
		Move::new(pos.x, pos.y, pos.x - 1, pos.y - 2),
		Move::new(pos.x, pos.y, pos.x + 2, pos.y + 1),
		Move::new(pos.x, pos.y, pos.x + 2, pos.y - 1),
		Move::new(pos.x, pos.y, pos.x - 2, pos.y + 1),
		Move::new(pos.x, pos.y, pos.x - 2, pos.y - 1),
	];

	for knight_move in knight_moves {
		if knight_move.b.x < 0 || knight_move.b.x >= chess.w {
			continue;
		}
		if knight_move.b.y < 0 || knight_move.b.y >= chess.h {
			continue;
		}

		if chess.fig_at(&knight_move.b).unwrap().name == 'N'
			&& chess.fig_at(&knight_move.b).unwrap().side != chess.sidemove
		{
			return true;
		}
	}

	false
}
