use crate::board::Chess;
use crate::figure_move::Pos;

impl std::fmt::Display for Chess {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		let mut result_str = String::new();

		for j in 0..self.h {
			for i in 0..self.w {
				result_str.push_str(&format!(
					"{} ",
					self.fig_at(&Pos {
						x: i,
						y: j
					})
					.unwrap()
					.name
				));
			}
			result_str.push_str("\n");
		}

		write!(f, "{}", &result_str)
	}
}
