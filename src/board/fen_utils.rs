use crate::figure::Figure;

use crate::board::Side;

use crate::board::{
	CASTLING_BLACK_KING, CASTLING_BLACK_QUEEN, CASTLING_WHITE_KING, CASTLING_WHITE_QUEEN,
};

pub fn board(fen: &str) -> Result<Vec<Vec<Figure>>, usize> {
	let board_lines: Vec<&str> = fen.split("/").collect();
	let mut board: Vec<Vec<Figure>> = Vec::new();

	let mut board_line_figures: Vec<Figure> = Vec::new();
	let mut figure: Figure;

	for board_line in board_lines {
		for figure_char in board_line.chars().collect::<Vec<char>>() {
			if figure_char.is_digit(10) {
				for _ in 0..figure_char.to_digit(10).unwrap() {
					board_line_figures.push(Figure::null());
				}

				continue;
			}

			let figure_side;
			match figure_char.is_uppercase() {
				true => {
					figure_side = Side::White;
				}
				false => {
					figure_side = Side::Black;
				}
			}

			match figure_char.to_ascii_uppercase() {
				'P' => {
					figure = Figure::pawn(figure_side);
				}
				'B' => {
					figure = Figure::bishop(figure_side);
				}
				'N' => {
					figure = Figure::knight(figure_side);
				}
				'R' => {
					figure = Figure::rook(figure_side);
				}
				'Q' => {
					figure = Figure::queen(figure_side);
				}
				'K' => {
					figure = Figure::king(figure_side);
				}
				_ => {
					return Err(69);
				}
			}

			board_line_figures.push(figure);
		}

		board.push(board_line_figures.clone());
		board_line_figures = Vec::new();
	}

	return Ok(board);
}

pub fn bit_castling(fen: &str) -> i8 {
	let mut bit_castling: i8 = 0i8;

	if fen.find('K').is_some() {
		bit_castling |= CASTLING_WHITE_KING;
	}
	if fen.find('Q').is_some() {
		bit_castling |= CASTLING_WHITE_QUEEN;
	}
	if fen.find('k').is_some() {
		bit_castling |= CASTLING_BLACK_KING;
	}
	if fen.find('q').is_some() {
		bit_castling |= CASTLING_BLACK_QUEEN;
	}

	bit_castling
}

pub fn bit_en_passant(fen: &str, board_w: i8, board_h: i8) -> Result<i8, ()> {
	let (x, y): (i8, i8);
	if fen != "-" {
		x = fen.as_bytes().as_ref()[0] as i8 - 'a' as i8;
		y = fen.chars().nth(1).unwrap().to_digit(10).unwrap() as i8 - 1;

		if x > board_w || y > board_h {
			return Err(());
		}
	} else {
		(x, y) = (0i8, 0i8);
	}

	Ok(x + y * board_w)
}
